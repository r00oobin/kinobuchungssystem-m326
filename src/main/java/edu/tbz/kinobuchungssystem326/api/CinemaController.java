package edu.tbz.kinobuchungssystem326.api;

import com.fasterxml.jackson.databind.JsonNode;
import edu.tbz.kinobuchungssystem326.model.Movie;
import edu.tbz.kinobuchungssystem326.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CinemaController {

    @Autowired
    private CinemaService service;


    @CrossOrigin
    @RequestMapping(path = "getShowsWithMovie/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonNode getAllShowsWithMovie(@PathVariable(name = "id") String id) {
        return service.getAllShowsWithMovie(Integer.valueOf(id));
    }

    @CrossOrigin
    @RequestMapping(path = "getAllMovies", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Movie> getAllMovies() {
        return service.getAllMovies();
    }

    @CrossOrigin
    @RequestMapping(path = "getAllSeats/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public JsonNode getAllSeats(@PathVariable(name = "id") String id) {
        return service.getSeats(Integer.valueOf(id));
    }
}
