package edu.tbz.kinobuchungssystem326.dao;

import edu.tbz.kinobuchungssystem326.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieDao extends JpaRepository<Movie, Integer> {
}
