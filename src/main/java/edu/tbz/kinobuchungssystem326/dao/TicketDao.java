package edu.tbz.kinobuchungssystem326.dao;

import edu.tbz.kinobuchungssystem326.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketDao extends JpaRepository<Ticket, Integer> {

    List<Ticket> getAllByShow_Id(Integer showId);
}
