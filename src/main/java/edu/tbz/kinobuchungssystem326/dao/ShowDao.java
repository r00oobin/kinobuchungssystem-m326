package edu.tbz.kinobuchungssystem326.dao;

import edu.tbz.kinobuchungssystem326.model.Movie;
import edu.tbz.kinobuchungssystem326.model.Show;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShowDao extends JpaRepository<Show, Integer> {

    List<Show> getAllByMovie_Id(Integer id);
}
