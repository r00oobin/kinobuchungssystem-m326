package edu.tbz.kinobuchungssystem326.service;

import com.fasterxml.jackson.databind.JsonNode;
import edu.tbz.kinobuchungssystem326.model.Movie;
import edu.tbz.kinobuchungssystem326.model.Show;

import java.util.List;


public interface CinemaService {

    List<Movie> getAllMovies();

    JsonNode getAllShowsWithMovie(Integer movieId);

    JsonNode getSeats(Integer showId);
}
