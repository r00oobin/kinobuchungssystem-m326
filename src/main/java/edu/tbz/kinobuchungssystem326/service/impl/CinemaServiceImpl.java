package edu.tbz.kinobuchungssystem326.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.tbz.kinobuchungssystem326.dao.MovieDao;
import edu.tbz.kinobuchungssystem326.dao.ShowDao;
import edu.tbz.kinobuchungssystem326.dao.TicketDao;
import edu.tbz.kinobuchungssystem326.model.*;
import edu.tbz.kinobuchungssystem326.service.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("cinemaServiceImpl")
public class CinemaServiceImpl implements CinemaService {


    @Autowired
    private MovieDao movieDao;

    @Autowired
    private TicketDao ticketDao;

    @Autowired
    private ShowDao showDao;

    private ObjectMapper oj;


    public CinemaServiceImpl(){
        this.oj = new ObjectMapper();
    }

    @Override
    public List<Movie> getAllMovies() {
        return movieDao.findAll();
    }

    @Override
    public JsonNode getAllShowsWithMovie(Integer movieId) {
        List<Show> shows = showDao.getAllByMovie_Id(movieId);
        ArrayNode array = oj.createArrayNode();
        for (Show s:shows){
            ObjectNode show = oj.createObjectNode();
            show.put("movie", s.getMovie().getName());
            show.put("hall", s.getHall().getName());
            show.put("start", s.getDatetime().toString());
            show.put("duration", s.getDuration());
            show.put("showId", s.getId());
            array.add(show);
        }
        return array;
    }

    @Override
    public JsonNode getSeats(Integer showId) {
        Show show = showDao.getOne(showId);
        List<Ticket> tickets = ticketDao.getAllByShow_Id(showId);
        ObjectNode object = oj.createObjectNode();
        object.put("movie", show.getMovie().getName());
        object.put("hall", show.getHall().getName());
        object.put("start", show.getDatetime().toString());
        object.put("duration", show.getDuration());

        ArrayNode arrayNode = oj.createArrayNode();

        for (Row row : show.getHall().getRows()){
            for (Seat seat : row.getSeats()){
                ObjectNode objectNode = oj.createObjectNode();
                objectNode.put("name", row.getName()+seat.getName());
                if (isInList(tickets, seat)){
                    Ticket ticket = getTicketForSeat(tickets, seat);
                    if (ticket.getStatus()){
                        objectNode.put("state", "bought");
                    }else {
                        objectNode.put("state", "reserved");

                    }
                }else {
                    objectNode.put("state", "available");
                }

                arrayNode.add(objectNode);
            }
        }
        object.putPOJO("seats", arrayNode);
        return object;
    }

    private boolean isInList(List<Ticket> tickets, Seat seat){
        for (Ticket ticket : tickets){
            if (ticket.getSeat().equals(seat)){
                return true;
            }
        }
        return false;
    }

    private Ticket getTicketForSeat(List<Ticket> tickets, Seat seat){
        for (Ticket ticket : tickets){
            if (ticket.getSeat().equals(seat)){
                return ticket;
            }
        }
        return null;
    }

}
