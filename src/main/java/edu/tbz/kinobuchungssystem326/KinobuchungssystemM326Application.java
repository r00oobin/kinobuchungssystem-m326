package edu.tbz.kinobuchungssystem326;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KinobuchungssystemM326Application {

    public static void main(String[] args) {
        SpringApplication.run(KinobuchungssystemM326Application.class, args);
    }
}
