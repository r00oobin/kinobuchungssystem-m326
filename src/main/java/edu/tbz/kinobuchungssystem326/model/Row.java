package edu.tbz.kinobuchungssystem326.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "t_row", schema = "cinema_management", catalog = "")
public class Row {
    private int id;
    private String name;
    private Hall hall;
    private List<Seat> seats;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Row row = (Row) o;
        return id == row.id &&
                Objects.equals(name, row.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_hall", referencedColumnName = "id", nullable = false)
    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    @OneToMany(mappedBy = "row")
    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
