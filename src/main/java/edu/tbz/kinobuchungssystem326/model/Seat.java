package edu.tbz.kinobuchungssystem326.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "t_seat", schema = "cinema_management", catalog = "")
public class Seat {
    private int id;
    private String name;
    private Row row;
    private List<Ticket> tickets;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Seat seat = (Seat) o;
        return id == seat.id &&
                Objects.equals(name, seat.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "fk_row", referencedColumnName = "id", nullable = false)
    public Row getRow() {
        return row;
    }

    public void setRow(Row row) {
        this.row = row;
    }

    @OneToMany(mappedBy = "seat")
    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
