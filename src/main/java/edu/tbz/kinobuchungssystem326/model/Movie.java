package edu.tbz.kinobuchungssystem326.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "t_movie", schema = "cinema_management", catalog = "")
public class Movie {
    private int id;
    private String name;
    private int duration;
    private String subtitle;
    private String ageRestriction;
    private List<Show> shows;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "duration", nullable = false)
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Basic
    @Column(name = "subtitle", nullable = false, length = 45)
    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Basic
    @Column(name = "ageRestriction", nullable = false, length = 45)
    public String getAgeRestriction() {
        return ageRestriction;
    }

    public void setAgeRestriction(String ageRestriction) {
        this.ageRestriction = ageRestriction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id == movie.id &&
                duration == movie.duration &&
                Objects.equals(name, movie.name) &&
                Objects.equals(subtitle, movie.subtitle) &&
                Objects.equals(ageRestriction, movie.ageRestriction);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, duration, subtitle, ageRestriction);
    }

    @JsonIgnore
    @OneToMany(mappedBy = "movie")
    public List<Show> getShows() {
        return shows;
    }

    public void setShows(List<Show> shows) {
        this.shows = shows;
    }
}
