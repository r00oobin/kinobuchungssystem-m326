package edu.tbz.kinobuchungssystem326.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_ticket", schema = "cinema_management", catalog = "")
public class Ticket {
    private int id;
    private double price;
    private boolean status;
    private Customer customer;
    private Seat seat;
    private Show show;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "price", nullable = false, precision = 0)
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return id == ticket.id &&
                Double.compare(ticket.price, price) == 0 &&
                status == ticket.status;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, price, status);
    }

    @ManyToOne
    @JoinColumn(name = "fk_customer", referencedColumnName = "id", nullable = false)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    @JoinColumn(name = "fk_seat", referencedColumnName = "id", nullable = false)
    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    @ManyToOne
    @JoinColumn(name = "fk_show", referencedColumnName = "id", nullable = false)
    public Show getShow() {
        return show;
    }

    public void setShow(Show show) {
        this.show = show;
    }
}
